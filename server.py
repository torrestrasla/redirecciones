import os
from http.server import SimpleHTTPRequestHandler
from staticserver.routes import routes
from staticserver.response.badRequestHandler import BadRequestHandler
from staticserver.response.staticHandler import StaticHandler
from staticserver.response.templateHandler import TemplateHandler
from http.server import HTTPServer


def corre():
    httpd = HTTPServer(("", 8000), Server)
    # print(time.asctime(), 'Server Starts - %s:%s' % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    # print(time.asctime(), 'Server Stops - %s:%s' % (HOST_NAME, PORT_NUMBER))


class Server(SimpleHTTPRequestHandler):

    def do_GET(self):
        split_path = os.path.splitext(self.path)
        print(str(split_path))
        request_extension = split_path[1]
        print(str(request_extension)+".")
        if request_extension is "" or request_extension is ".html":
            if self.path in routes:
                #file = open('templates/{}'.format(routes[self.path]['template']))
                handler = TemplateHandler()
                handler.find(routes[self.path])
                print(handler.getContents())
            else:
                handler = BadRequestHandler()
        elif request_extension is ".py":
            handler = BadRequestHandler()
        else:
            handler = StaticHandler()
            handler.find(self.path)

        self.respond({
            'handler': handler
        })

    def handle_http(self, handler):
        status = handler.getStatus()
        self.send_response(status)
        print(status)
        if status is 200:
            content = handler.getContents()
            self.send_header('Content-type', handler.getContentType())
        else:
            content = '404 Not Found'

        self.end_headers()
        return bytes(content, 'UTF-8')

    def respond(self, opts):
        response = self.handle_http(opts['handler'])
        self.wfile.write(response)




corre()
